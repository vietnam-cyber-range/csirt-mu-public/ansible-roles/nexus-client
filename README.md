# Ansible role - Nexus-client

This role sets package managers to use CSIRT-MU Nexus repositories

By default sets following package managers:
* apt
* mvn
* npm
* pip

## Requirements

* This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

    ```yml
    become: yes
    ```

* Package maven for mvn configuration.
* Package nodejs for npm configuration.

## Role paramaters

Optional parameters.

* `nexus_client_apt_distributions` - The list of APT distributions to configure (default: ["jessie", "stretch"]).
* `nexus_client_repositories` - The list of managers to configure (default: ["apt", "mvn", "npm", "pip"]).

## Example

Example of the simplest Nexus client configuration.

```yml
roles:
    - role: nexus-client
```
